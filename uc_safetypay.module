<?php


define('UC_SAFETYPAY_SANDBOX_URL',      'https://mws2.safetypay.com/Sandbox/express/ws/v.3.0/Post/CreateExpressToken');
define('UC_SAFETYPAY_LIVE_URL',         'https://mws2.safetypay.com/express/ws/v.3.0/Post/CreateExpressToken');
define('UC_SAFETYPAY_OK_URL',           'cart/checkout/complete');
define('UC_SAFETYPAY_ERROR_URL',        'uc_safetypay/failed');
define('UC_SAFETYPAY_STATUS_SANDBOX',   1);
define('UC_SAFETYPAY_STATUS_PROD',      0);
define('UC_SAFETYPAY_HTTPS_PROTOCOL',   'https');
define('UC_SAFETYPAY_DEF_LANGUAGE',     'EN'); // SafetyPay supports: 'ES', 'EN', 'DE' or 'PT'
define('UC_SAFETYPAY_DEF_CURRENCY',     'USD'); // SafetyPay supports: 'USD', 'PEN', 'MXN', 'EUR', 'CRC', 'BRL' or 'CAD'
define('UC_SAFETYPAY_DEF_EXPIRATION',   15);
define('UC_SAFETYPAY_DEF_RESPONSE',     'XML'); // SafetyPay supports: 'XML' or 'CSV'


/**
 * Implements hook_menu().
 */
function uc_safetypay_menu() {
  $items = array();

  $items['uc_safetypay/failed'] = array(
    'title' => 'Payment process failed',
    'page callback' => 'uc_safetypay_payment_failed',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'includes/uc_safetypay.services.inc',
  );

  $items['uc_safetypay/notification'] = array(
    'title' => 'Transaction Notification',
    'page callback' => 'uc_safetypay_payment_ipn_notification',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'includes/uc_safetypay.services.inc',
  );

  return $items;
}

/**
 * Implements hook_uc_payment_method().
 */
function uc_safetypay_uc_payment_method() {
  $methods = array();

  $methods[] = array(
    'id' => 'uc_safetypay',
    'name' => t('SafetyPay'),
    'title' => t('SafetyPay'),
    'review' => t('SafetyPay'),
    'desc' => t('Redirect users to submit payments through SafetyPay Gateway'),
    'callback' => 'uc_safetypay_payment_method_callback',
    'weight' => 1,
    'checkout' => TRUE,
    'no_gateway' => TRUE,
  );

  return $methods;
}

/**
 * Implements hook_uc_payment_method_callback().
 */
function uc_safetypay_payment_method_callback($op, &$order, $form = NULL, &$form_state = NULL) {
  switch ($op) {
    case 'cart-details':
      return array('#markup' => t('Continue with checkout to complete payment.'));

    case 'settings':
      $form = uc_safetypay_payment_settings_form();

      return $form;
  }
}

/**
 * Settings form callback function for Ubercart SafetyPay payment method
 */
function uc_safetypay_payment_settings_form() {
  $form = array();

  $form['uc_safetypay_env'] = array(
    '#type' => 'select',
    '#title' => t('Environment'),
    '#options' => array(
      UC_SAFETYPAY_STATUS_SANDBOX => t('Sandbox'),
      UC_SAFETYPAY_STATUS_PROD => t('Production'),
    ),
    '#default_value' => variable_get('uc_safetypay_env', UC_SAFETYPAY_STATUS_SANDBOX),
  );

  $form['uc_safetypay_sandbox_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Sandbox URL for POST Request'),
    '#description' => t('Warning! Be careful changing this URL. Change this value only if you know what are you doing'),
    '#default_value' => variable_get('uc_safetypay_sandbox_url', UC_SAFETYPAY_SANDBOX_URL),
  );

  $form['uc_safetypay_live_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Production URL for POST Request'),
    '#description' => t('Warning! Be careful changing this URL. Change this value only if you know what are you doing'),
    '#default_value' => variable_get('uc_safetypay_live_url', UC_SAFETYPAY_LIVE_URL),
  );

  $form['uc_safetypay_protocol'] = array(
    '#type' => 'select',
    '#title' => t('Communication Protocol'),
    '#options' => array(
      UC_SAFETYPAY_HTTPS_PROTOCOL => t('HTTPS'),
    ),
    '#default_value' => variable_get('uc_safetypay_protocol', UC_SAFETYPAY_HTTPS_PROTOCOL),
  );

  $form['uc_safetypay_language'] = array(
    '#type' => 'select',
    '#title' => t('Language'),
    '#options' => array(
      'ES' => t('Spanish'),
      'EN' => t('English'),
      'DE' => t('Deutsch'),
      'PT' => t('Portuguese'),
    ),
    '#default_value' => variable_get('uc_safetypay_language', UC_SAFETYPAY_DEF_LANGUAGE),
  );

  $form['uc_safetypay_currency'] = array(
    '#type' => 'select',
    '#title' => t('Currency Code'),
    '#options' => array(
      'USD' => t('USD, US Dollar'),
      'PEN' => t('PEN, Nuevos Soles'),
      'MXN' => t('MXN, Mexico Pesos'),
      'EUR' => t('EUR, Euro'),
      'CRC' => t('CRC, Costa Rica Colon'),
      'BRL' => t('BRL, Brazil Real'),
      'CAD' => t('CAD, Canadian Dollar'),
    ),
    '#default_value' => variable_get('uc_safetypay_currency', UC_SAFETYPAY_DEF_CURRENCY),
  );

  $form['uc_safetypay_expiration'] = array(
    '#type' => 'textfield',
    '#title' => t('Expiration Time'),
    '#description' => t('Enter expiration time in minutes (Min: 5 minutes, Max: 1440 minutes, 1 day)'),
    '#max_length' => 5,
    '#size' => 5,
    '#default_value' => variable_get('uc_safetypay_expiration', UC_SAFETYPAY_DEF_EXPIRATION),
  );

  $form['uc_safetypay_response'] = array(
    '#type' => 'select',
    '#title' => t('Response format'),
    '#options' => array(
      'XML' => t('XML'),
      'CSV' => t('CSV'),
    ),
    '#default_value' => variable_get('uc_safetypay_response', UC_SAFETYPAY_DEF_RESPONSE),
  );

  $form['uc_safetypay_merchant'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant ID'),
    '#default_value' => variable_get('uc_safetypay_merchant', ''),
  );

  $form['uc_safetypay_apikey'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#default_value' => variable_get('uc_safetypay_apikey', ''),
    '#maxlength' => 32,
  );

  $form['uc_safetypay_signature'] = array(
    '#type' => 'textfield',
    '#title' => t('Signature Key'),
    '#default_value' => variable_get('uc_safetypay_signature', ''),
    '#maxlength' => 32,
  );

  $form['uc_safetypay_ok_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Transaction OK URL'),
    '#default_value' => variable_get('uc_safetypay_ok_url', UC_SAFETYPAY_OK_URL),
    '#field_prefix' => $GLOBALS['base_url'] . '/',
  );

  $form['uc_safetypay_error_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Transaction ERROR URL'),
    '#default_value' => variable_get('uc_safetypay_error_url', UC_SAFETYPAY_ERROR_URL),
    '#field_prefix' => $GLOBALS['base_url'] . '/',
  );

  return $form;
}

/**
 * Implements hook_form_FORM_ID_alter() for uc_cart_checkout_review_form().
 */
function uc_safetypay_form_uc_cart_checkout_review_form_alter(&$form, &$form_state, $form_id) {
  if ($form_state['build_info']['args'][0]->payment_method == 'uc_safetypay') {
    $form['#submit'][] = 'uc_safetypay_review_form_submit'; // Added new submit callback to make POST request to SafetyPay
  }
}

/**
 * Additional submit handler for uc_cart_checkout_review_form().
 *
 * @see uc_cart_checkout_review_form()
 */
function uc_safetypay_review_form_submit($form, &$form_state) {
  $order = uc_order_load(intval($_SESSION['cart_order']));
  $curr_datetime = uc_safetypay_get_dateISO8601(time());
  $amount = number_format($order->order_total, 2, '.', '');

  $request = array(
    'ApiKey' => variable_get('uc_safetypay_apikey', ''),
    'RequestDateTime' => $curr_datetime,
    'CurrencyCode' => variable_get('uc_safetypay_currency', UC_SAFETYPAY_DEF_CURRENCY),
    'Amount' => $amount,
    'MerchantSalesID' => "ORDER-$order->order_id",
    'Language' => variable_get('uc_safetypay_language', UC_SAFETYPAY_DEF_LANGUAGE),
    'TrackingCode' => '', // Not required
    'ExpirationTime' => variable_get('uc_safetypay_expiration', UC_SAFETYPAY_DEF_EXPIRATION),
    'TransactionOkURL' => $GLOBALS['base_url'] . '/' . variable_get('uc_safetypay_ok_url', UC_SAFETYPAY_OK_URL),
    'TransactionErrorURL' => $GLOBALS['base_url'] . '/' . variable_get('uc_safetypay_error_url', UC_SAFETYPAY_ERROR_URL),
    'ResponseFormat' => variable_get('uc_safetypay_response', UC_SAFETYPAY_DEF_RESPONSE),
  );

  $request['Signature'] = uc_safetypay_get_signature($request);
  $response = uc_safetypay_api_request($request);

  if ($response['ErrorNumber'] == 0) {
    header('Location: ' . $response['ClientRedirectURL']);
    exit();
  }
  else {
    drupal_set_message(t('Error message from SafetyPay: %message', array('%message' => $response['ErrorDescription'])), 'error');
    $form_state['redirect'] = 'cart/checkout';
  }
}

/**
 * Sends a request to SafetyPay and returns a response array.
 */
function uc_safetypay_api_request($request) {
  if (variable_get('uc_safetypay_env', UC_SAFETYPAY_STATUS_SANDBOX) == UC_SAFETYPAY_STATUS_SANDBOX) {
    $server = variable_get('uc_safetypay_sandbox_url', UC_SAFETYPAY_SANDBOX_URL);
  }
  else {
    $server = variable_get('uc_safetypay_live_url', UC_SAFETYPAY_LIVE_URL);
  }

  $data = '';
  foreach ($request as $key => $value) {
    $data .= $key . '=' . urlencode(str_replace(',', '', $value)) . '&';
  }
  $data = substr($data, 0, -1);

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $server);
  curl_setopt($ch, CURLOPT_VERBOSE, FALSE);
  curl_setopt($ch, CURLOPT_POST, TRUE);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($ch, CURLOPT_NOPROGRESS, TRUE);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
  $response = curl_exec($ch);

  if ($error = curl_error($ch)) {
    watchdog('uc_safetypay', '!error', array('!error' => $error), WATCHDOG_ERROR);
  }

  curl_close($ch);

  return _uc_safetypay_response_data($response);
}

/**
 * Turns SafetyPay's response to an associative array depending on the response format.
 */
function _uc_safetypay_response_data($response) {
  $type = variable_get('uc_safetypay_response', UC_SAFETYPAY_DEF_RESPONSE);

  switch ($type) {
    case ('XML'):
      $xml = simplexml_load_string($response);
      if ((int) $xml->ExpressTokenResponse->ErrorManager->ErrorNumber == 0) {
        $data = array(
          'ErrorNumber' => (string) $xml->ExpressTokenResponse->ErrorManager->ErrorNumber,
          'ResponseDateTime' => (string) $xml->ExpressTokenResponse->ResponseDateTime,
          'ClientRedirectURL' => (string) $xml->ExpressTokenResponse->ShopperRedirectURL,
          'Signature' => (string) $xml->ExpressTokenResponse->Signature,
        );
      }
      else {
        $data = array(
          'ErrorNumber' => (string) $xml->ExpressTokenResponse->ErrorManager->ErrorNumber,
          'Severity' => (string) $xml->ExpressTokenResponse->ErrorManager->Severity,
        );
      }
      break;

    case 'CSV':
      list($error, $date, $URL, $signature) = explode(',', $response);
      if ($error == 0) {
        $data = array(
          'ErrorNumber' => $error,
          'ResponseDateTime' => $date,
          'ClientRedirectURL' => $URL,
          'Signature' => $signature,
        );
      }
      else {
        $data = array(
          'ErrorNumber' => $error,
          'Severity' => $URL,
        );
      }
      break;
  }

  $data['ErrorDescription'] = _uc_safetypay_get_error($data['ErrorNumber']);

  return $data;
}

/**
 * Returns a description of the error returned from SafetyPay depending on the ErrorNumber.
 */
function _uc_safetypay_get_error($err_num) {
  switch($err_num) {
    case '0':
      $error = t('No errors');
      break;
    case '1':
      $error = t('Credentials not valid');
      break;
    case '2':
      $error = t('Error in Signature');
      break;
    case '100100':
      $error = t('General Error');
      break;
    case '100101':
      $error = t('Service not allowed');
      break;
    case '100102':
      $error = t('Error in Currency Code');
      break;
    case '100103':
      $error = t('Currency not allowed');
      break;
    case '100104':
      $error = t('Error in number of digits');
      break;
    case '100105':
      $error = t('Amount must be different than zero');
      break;
    case '20103':
      $error = t('Refund amount not allowed');
      break;
    case '20104':
      $error = t('Original Transaction Date out of range');
      break;
  }

  return $error;
}

/**
 * Response of POST notification to send to SafetyPay.
 */
function _uc_safetypay_POST_response_data($request, $num_error) {
  $curr_datetime = uc_safetypay_get_dateISO8601(time());

  list($tmp, $cart_id) = explode('-', check_plain($_POST['MerchantSalesID']));

  $response = array(
    'ErrorNumber' => $num_error,
    'ResponseDateTime' => $curr_datetime,
    'MerchantSalesID' => $request['MerchantSalesID'],
    'ReferenceNo' => $request['ReferenceNo'],
    'CreationDateTime' => $request['CreationDateTime'],
    'Amount' => $request['Amount'],
    'CurrencyID' => $request['CurrencyID'],
    'PaymentReferenceNo' => $request['PaymentReferenceNo'],
    'Status' => $request['Status'],
    'OrderNo' => $cart_id,
  );

  $response['Signature'] = uc_safetypay_get_signature($response, 'post_response');
  watchdog('uc_safetypay', '2. Response data to SafetyPay: @msg', array('@msg' => implode('|', $response)), WATCHDOG_NOTICE);

  return implode(',', $response);
}

/**
 * Get a SHA256 Signature to get/send from/on the SafetyPay request. 
 */
function uc_safetypay_get_signature($request, $op_key = 'token') {
  module_load_include('inc', 'uc_safetypay', 'includes/nanolink-sha256');

  switch ($op_key) {
    case 'token': // Get signature to send on POST request to get SafetyPay Express Token
      $data_keys = array(
        'RequestDateTime', 'CurrencyCode', 'Amount', 'MerchantSalesID', 'Language',
        'TrackingCode', 'ExpirationTime', 'TransactionOkURL', 'TransactionErrorURL'
      );
      break;

    case 'post': // Get signature to compare with signature received on POST notification
      $data_keys = array(
        'RequestDateTime', 'MerchantSalesID', 'ReferenceNo', 'CreationDateTime',
        'Amount', 'CurrencyID', 'PaymentReferenceNo', 'Status'
      );
      break;

    case 'post_response': // Get signature to send on POST response to SafetyPay
      $data_keys = array(
        'ResponseDateTime', 'MerchantSalesID', 'ReferenceNo', 'CreationDateTime',
        'Amount', 'CurrencyID', 'PaymentReferenceNo', 'Status', 'OrderNo'
      );
      break;
  }

  $params = '';
  foreach ($request as $key => $value) {
    if (in_array($key, $data_keys)) {
      $params .= trim($value);
    }
  }

  $signature = sha256($params . variable_get('uc_safetypay_signature', ''));

  return $signature;
}

/**
 * Get a date in ISO8601 format, necessary for SafetyPay requests.
 */
function uc_safetypay_get_dateISO8601($int_date) {
  $date_mod = date('Y-m-d\TH:i:s', $int_date);
  $pre_timezone = date('O', $int_date);
  $time_zone = substr($pre_timezone, 0, 3) . ':' . substr($pre_timezone, 3, 2);

  $pos = strpos($time_zone, "-");

  if (PHP_VERSION >= '4.0') {
    if ($pos === false) {
      // nothing
    }
    else {
      if ($pos != 0) {
        $date_mod = $time_zone;
      }
      else {
        if (is_string($pos) && !$pos) {
          // nothing
        }
        else {
          if ($pos != 0) {
            $date_mod = $time_zone;
          }
        }
      }
    }
  }

  return $date_mod;
}
