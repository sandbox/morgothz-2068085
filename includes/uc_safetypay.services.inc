<?php

/**
 * Callback to handle a failed payment from SafetyPay
 */
function uc_safetypay_payment_failed() {
  watchdog('uc_safetypay', 'Safetypay encountered an error during the payment process of order @order_id', array('@order_id' => $_SESSION['cart_order']), WATCHDOG_ERROR);

  drupal_set_message(t('Safetypay encountered an error during the payment process of your order'), 'error');
  drupal_goto('cart');
}

/**
 * Processes Instant Payment Notifiations from SafetyPay.
 */
function uc_safetypay_payment_ipn_notification() {
  $reqItems = array(
    'ApiKey' => strip_tags($_POST['ApiKey']),
    'RequestDateTime' => strip_tags($_POST['RequestDateTime']),
    'MerchantSalesID' => strip_tags($_POST['MerchantSalesID']),
    'ReferenceNo' => strip_tags($_POST['ReferenceNo']),
    'CreationDateTime' => strip_tags($_POST['CreationDateTime']),
    'Amount' => strip_tags($_POST['Amount']),
    'CurrencyID' => strip_tags($_POST['CurrencyID']),
    'PaymentReferenceNo' => strip_tags($_POST['PaymentReferenceNo']),
    'Status' => strip_tags($_POST['Status']),
    'Signature' => strip_tags($_POST['Signature'])
  );

  if (!isset($_POST['MerchantSalesID'])) {
    watchdog('uc_safetypay', 'IPN attempted with invalid order ID.', array(), WATCHDOG_ERROR);
    return;
  }

  list($tmp, $cart_id) = explode('-', check_plain($_POST['MerchantSalesID']));
  if (!empty($cart_id)) {
    // Needed later by uc_complete_sale to empty the correct cart
    $_SESSION['uc_cart_id'] = $cart_id;
  }

  $order = uc_order_load($cart_id);

  if ($order == FALSE) {
    watchdog('uc_safetypay', 'IPN attempted for non-existent order @order_id.', array('@order_id' => $cart_id), WATCHDOG_ERROR);
    return;
  }


  if ($order->order_status == 'payment_received' || $order->order_status == 'completed') {
    watchdog('uc_safetypay', 'It has already been received a payment for this order: @order_id', array('@order_id' => $order->order_id), WATCHDOG_NOTICE);
    return;
  }

  watchdog('uc_safetypay', '1. SafetyPay data sent in POST notification: @msg', array('@msg' => implode('|', $reqItems)), WATCHDOG_NOTICE);

  $Signature = uc_safetypay_get_signature($reqItems, 'post');

  if (strtoupper($reqItems['ApiKey']) != strtoupper(variable_get('uc_safetypay_apikey', ''))) {
    $sDataResponse = t("Error (1) in ApiKey.");
    $num_error = 1;
  }

  if (strtoupper($reqItems['Signature']) != strtoupper($Signature)) {
    $sDataResponse = t("Error (2) in Signature");
    $num_error = 2;
  }

  $payment_id = check_plain($_POST['PaymentReferenceNo']);
  $payment_currency = check_plain($_POST['CurrencyID']);
  $payment_amount = check_plain($_POST['Amount']);
  $payment_status = check_plain($_POST['Status']);

  switch($payment_status) {
    case '101': // Transaction begins
      uc_order_update_status($order->order_id, 'pending');
      uc_order_comment_save($order->order_id, 0, t('SafetyPay IPN reported a pending status for your order'), 'order', $order->order_status);
      break;

    case '102': // SafetyPay received confirmation of payment of a Associated Bank
      if (abs($payment_amount - $order->order_total) > 0.01) {
        watchdog('uc_safetypay', 'Payment @payment_id for order @order_id did not equal the order total.',
          array('@payment_id' => $payment_id, '@order_id' => $order->order_id), WATCHDOG_WARNING, l(t('view'), 'admin/store/orders/' . $order->order_id));
      }

      $num_error = 0;
      $comment = t('SafetyPay transaction ID: @payment_id', array('@payment_id' => $payment_id));
      uc_payment_enter($order->order_id, 'uc_safetypay', $payment_amount, $order->uid, NULL, $comment);
      uc_cart_complete_sale($order);
      uc_order_comment_save($order->order_id, 0, t('SafetyPay IPN reported a payment of @amount @currency.',
        array('@amount' => uc_currency_format($payment_amount, FALSE), '@currency' => $payment_currency)), 'order', $order->order_status);
      break;
  }

  $sDataResponse = _uc_safetypay_POST_response_data($reqItems, $num_error);

  watchdog('uc_safetypay', '3. Response End: @msg', array('@msg' => $sDataResponse), WATCHDOG_NOTICE);

  // Write response to SafetyPay
  echo $sDataResponse;
}
